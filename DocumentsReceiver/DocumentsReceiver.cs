﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Receiver
{
   public delegate void FilesLoadedEventHandler();
   public delegate void TimedOutEventHandler();

   public class DocumentsReceiver
   {
      public event FilesLoadedEventHandler DocumentsReady;
      public event Action<string> DocumentUploaded;
      public event TimedOutEventHandler TimedOut;

      Dictionary<string, bool> _files;
      Timer _timer;
      FileSystemWatcher _watcher;

      public DocumentsReceiver()
      {
         _timer   = new Timer();
         _watcher = new FileSystemWatcher();
         _files   = new Dictionary<string, bool>();
      }

      public void Start(long waitingInterval, string targetDirectory, List<string> documents)
      {
         _watcher.Path = targetDirectory;
         _timer.Interval = waitingInterval;
         documents.ForEach(f => _files.Add(f, false));
         _timer.Elapsed += OnTimeElapsed;
         _watcher.EnableRaisingEvents = true;
         _watcher.Created += OnFileCreated;
         _watcher.Renamed += OnFileCreated;
         _watcher.Deleted += OnFileDeleted;
         _timer.Start();
      }

      private void OnTimeElapsed(object sender, ElapsedEventArgs e)
      {
         TimedOut?.Invoke();
         UnSubscribe();
      }

      private void OnFileCreated(object sender, FileSystemEventArgs e)
      {
         DocumentUploaded?.Invoke(e.Name);
         MarkFileAsAdded(e.Name);
         if (AllFilesReceived())
         {
            DocumentsReady?.Invoke();
            UnSubscribe();
         }
      }

      private void OnFileDeleted(object sender, FileSystemEventArgs e)
      {
         if (_files.Any(a => a.Key == e.Name))
         {
            _files[e.Name] = false;
         }
      }

      void MarkFileAsAdded(string fileName)
      {
         if (_files.Any(a => a.Key == fileName))
         {
            _files[fileName] = true;
         }
      }

      void UnSubscribe()
      {
         _timer.Elapsed -= OnTimeElapsed;
         _watcher.Created -= OnFileCreated;
         _timer.Stop();
         _timer.Dispose();
         _watcher.Dispose();
      }

      bool AllFilesReceived()
         => _files.Select(s => s.Value).All(a => a == true);
   }
}
