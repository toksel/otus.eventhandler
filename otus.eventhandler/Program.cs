﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Receiver;

namespace otus.eventhandler
{
   class Program
   {
      static void Main(string[] args)
      {
         string path = "D:\\test";
         List<string> files = new() { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
         long intervalMs = 10000;
         DocumentsReceiver doc = new();

         doc.DocumentsReady += DocumentsUploaded;
         doc.DocumentUploaded += DocumentUploaded;
         doc.TimedOut += TimeOutError;

         doc.Start(intervalMs, path, files);

         while (true)
         {
            if (files.Any())
            {
               Console.WriteLine("Chose file you want to add to direcotry:");
            }
            for (int i = 1; i < files.Count + 1; i++)
            {
               Console.WriteLine($"{i} - {files[i - 1]}");
            }
            Console.WriteLine("0 - Exit");

            int input = GetNumericInput((a) => a >= 0 && a < files.Count + 1);
            UploadFile(path, files[input - 1]);
            files.Remove(files[input - 1]);
            Console.WriteLine();
         }

         Console.ReadKey();
      }

      static void DocumentsUploaded()
      {
         Console.WriteLine("Documents have been successfully uploaded!");
      }

      static void DocumentUploaded(string name)
      {
         Console.WriteLine($"File {name} has been uploaded");
      }

      static void TimeOutError()
      {
         Console.WriteLine("Your time is up. Try again.");
      }

      static int GetNumericInput(Predicate<int> predicate)
      {
         int inputValue = 99;
         bool correctValue = false;
         while (!correctValue)
         {
            correctValue = int.TryParse(Console.ReadLine(), out inputValue) && predicate(inputValue);
            if (inputValue == 0)
            {
               Environment.Exit(0);
            }
            if (!correctValue)
            {
               Console.WriteLine("Incorrect value");
            }
         }
         return inputValue;
      }

      static void UploadFile(string directory, string fileName)
         => File.Create(Path.Combine(directory, fileName));
   }
}